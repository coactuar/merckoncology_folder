<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $emailid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$emailid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_emailid"]);
            unset($_SESSION["user_loc"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="top">
        <img src="img/top.jpg" width="100%" alt=""/> 
</div>
<div class="container-fluid">
    <div class="row mt-1">
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center">
                <h4>Virtual Advisory Board Meeting in Oncology</h4>
                <br>
                <h6>Date: 2nd August 2020</h6>
                <h6>Time: 7.00pm</h6>
        </div>
        <div class="col-12 col-md-4 col-lg-3 ">
            <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    
    <div class="row video-panel">
        <div class="col-12 col-md-8 offset=md-2 col-lg-6 offset-lg-3">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no"></iframe>
            </div>    
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>