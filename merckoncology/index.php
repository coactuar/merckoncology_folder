<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="top">
        <img src="img/top.jpg" width="100%" alt=""/> 
</div>
<div class="container-fluid">
    <div class="row mt-1">
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center">
                <h4>Virtual Advisory Board Meeting in Oncology</h4>
                <br>
                <h6>Date: 2nd August 2020</h6>
                <h6>Time: 7.00pm</h6>
        </div>
        <div class="col-12 col-md-4 col-lg-3 ">
            <a href="agenda.pdf" target="_blank" download class="btn btn-sm btn-danger">Download Agenda</a>
        </div>
    </div>
    <div class="row ">
        <div class="col-8 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-2">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="row mt-1">
                    <div class="col-12 col-md-3">
                    Full Name
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" class="form-control" placeholder="Full Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-12 col-md-3">
                    Location
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" class="form-control" placeholder="Location" name="location" id="location" required>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-12 col-md-3">
                    Email
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="email" class="form-control" placeholder="Email" name="emailid" id="emailid" required>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-12 col-md-3">
                    Disclaimer
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="checkbox" name="disclaimer" id="disclaimer" required>
                        <textarea type="text" class="form-control" rows="5" >"This communication and the event / meeting referenced therein are for Registered Medical Practitioners only. The communication and the event / meeting referenced therein are for academic and educational purposes only. They may refer to pharmaceutical products, diagnostic techniques, therapeutics or indications not yet registered or approved in a given country and it should be noted that, over time, currency and completeness of the data may change. For updated information, please contact the Company. They should not be used to diagnose, treat, cure or prevent any disease or condition without the professional advice of a Registered Medical Practitioner, and does not replace medical advice or a thorough medical examination. Registered Medical Practitioners should use their independent professional judgement in checking the symptoms, diagnosing & suggesting the appropriate line of treatment for patients. The information provided by or about the websites / programs is not exhaustive and shall in no way be construed as advertisement and is only provided for general awareness. The decisions should not be based on the information appearing on this website and one should personally evaluate and satisfy oneself before visiting any website / participating in any program. Acceptance of the invitation and participation in the event / meeting are voluntary and the Registered Medical Practitioners are not under any obligation whatsoever to accept the invitation and to participate in the event / meeting. Merck is not in any way influencing, propagating or inducing anyone to buy or use Merck products. Merck accepts no liability for any loss, damage or compensation claims in connection with any act or omission by anyone based on information contained in or derived through use of this document. Duplication of or copying any data / content requires prior permission of the copyright holder."</textarea>
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-12 col-md-3">
                    </div>
                    <div class="col-12 col-md-9">
                        <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                    </div>
                  </div>
                 <div class="input-group">
                    
                  </div>
                
            </form>        
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>